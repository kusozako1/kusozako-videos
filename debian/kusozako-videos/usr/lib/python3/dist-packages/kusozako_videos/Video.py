# (c) copyright 2022, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk
from kusozako1.Entity import DeltaEntity
from kusozako1.const import MainWindowSignals


# class DeltaVideo(Gtk.Video, DeltaEntity):
class DeltaVideo(Gtk.Video, DeltaEntity):

    def receive_transmission(self, user_data):
        signal, gfile = user_data
        if signal != MainWindowSignals.FILE_OPENED:
            return
        self._media_file.pause()
        self._media_file.set_file(gfile)
        self._media_file.play()

    def __init__(self, parent):
        self._parent = parent
        Gtk.Video.__init__(self, hexpand=True, vexpand=True)
        self._media_file = Gtk.MediaFile.new()
        self._media_file.props.volume = 0.2
        self.set_media_stream(self._media_file)
        files = self._enquiry("delta > command line files")
        if files:
            self.set_file(files[0])
        self._raise("delta > add to container", self)
        self._raise("delta > register main window signal object", self)
