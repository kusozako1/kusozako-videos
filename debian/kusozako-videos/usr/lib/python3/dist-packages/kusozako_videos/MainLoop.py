# (c) copyright 2022, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from kusozako1.main_loop.MainLoop import AlfaMainLoop
from . import APPLICATION_DATA
from .Video import DeltaVideo


class DeltaMainLoop(AlfaMainLoop):

    def _delta_call_loopback_application_window_ready(self, parent):
        DeltaVideo(parent)

    def _delta_info_data(self, key):
        return APPLICATION_DATA.get(key, None)
