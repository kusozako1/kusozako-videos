# (c) copyright 2022, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

import gettext
import locale
from kusozako1.const import UserSpecialDirectories

VERSION = "2022.12.10"
APPLICATION_NAME = "kusozako-videos"
APPLICATION_ID = "com.gitlab.kusozako1.Videos"

locale.setlocale(locale.LC_ALL, None)
gettext.install(
    APPLICATION_NAME,
    "/usr/share/locale",
    names=('gettext', 'ngettext')
)

LONG_DESCRIPTION = "Video viwer for kusozako project."

COPYRIGHT = """Copyright © 2022
takeda.nemuru<takeda.nemuru@protonmail.com>"""

APPLICATION_DATA = {
    "version": VERSION,
    "min-files": 0,
    "max-files": 1,
    "mime": "video/*",
    "default-file-directory": UserSpecialDirectories.VIDEOS,
    "application-name": APPLICATION_NAME,
    "rdnn-name": APPLICATION_ID,
    "application-id": APPLICATION_ID,
    "long-description": LONG_DESCRIPTION,
    "home-page": "https://www.gitlab.com/kusozako1/kusozako-videos",
    "copyright": COPYRIGHT,
    "license": "GPL-3.0-or-later",
}
